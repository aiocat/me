var shell = document.getElementById("shell");
shell.onkeydown = shellHandler

var commands = [
    {
        name: "help",
        description: "Gets informations about commands",
        usage: "help [command]",
        handler: helpHandler
    },
    {
        name: "clear",
        description: "Clears the output",
        usage: "clear",
        handler: clearOutput
    },
    {
        name: "about",
        description: "Shows about text",
        usage: "about",
        handler: aboutHandler
    },
    {
        name: "projects",
        description: "Shows current and done projects",
        usage: "projects",
        handler: projectsHandler
    },
    {
        name: "social",
        description: "Shows social media accounts",
        usage: "social",
        handler: socialHandler
    }
]

function shellHandler(e) {
    if (e.code == "Enter") {
        clearOutput()
        let args = shell.value.split(" ")
        let result = commands.find(v => v.name == args[0])
        if (!result) {
            let warn = document.createElement("div")
            warn.className = "warn"
            warn.innerText = "Command not found"
            warn.onclick = removeWarn

            document.getElementById("warn-stack").appendChild(warn)
            shell.value = ""
            return
        } else {
            result.handler(args.slice(1))
            shell.value = ""
        }
    }
}

function writeOutput(text) {
    document.getElementById("output").innerText += text
}

function writeUnsafeOutput(text) {
    document.getElementById("output").innerHTML += text
}

function clearOutput() {
    document.getElementById("output").innerText = ""
}

function helpHandler(args) {
    if (args.length == 0) {
        writeOutput("{parameter} | [optional parameter]\n\n")
        commands.forEach(v => {
            writeOutput(`${v.usage}: ${v.description}\n`)
        })
    } else {
        let result = commands.find(v => v.name == args[0])

        if (!result) {
            let warn = document.createElement("div")
            warn.className = "warn"
            warn.innerText = "Command not found"
            warn.onclick = removeWarn

            document.getElementById("warn-stack").appendChild(warn)
        } else writeOutput(`${result.usage}: ${result.description}\n`)
    }
}

function aboutHandler(_) {
    writeOutput(`Hello, Welcome to my page!
I'm currently 16 years old. I like engineering stuffs.
I want to be a software engineer in the future.
I also do 3D and 2D Arts as a hobbie.
I forgot to mention, I love cats alot!`)
}

function socialHandler(_) {
    writeOutput(`
Discord: 872202276297637928
Twitter: aiocat420
Github: aiocat
Gitlab: aiocat
Osu: 111sec
Deviantart: aiocat`)
}

function projectsHandler(_) {
    writeUnsafeOutput(`
<a href="https://gitlab.com/aiocat/virna" target="_blank">Virna:</a> A programming language designed to be stack-based and concatenative with reverse polish notation.
<br>
<a href="https://gitlab.com/aiocat/snug" target="_blank">Snug:</a> Most minimalistic config file language.
<br>
<a href="https://gitlab.com/modeminal/libraries" target="_blank">Modeminal:</a> Bloat-free rust libraries.
<br>
<a href="https://gitlab.com/aiocat/webcord" target="_blank">Webcord:</a> Theme injector extension for web version of the discord.
<br>
<a href="https://gitlab.com/aiocat/solute" target="_blank">Solute:</a> Generates unique github-like default avatar based on nickname (rust).
<br>
<a href="https://gitlab.com/aiocat/bfmod" target="_blank">BFMOD:</a> Brainfuck lexer and compiler library for rust.
<br>
<a href="https://gitlab.com/aiocat/jdw" target="_blank">JDW:</a> Dynamic windows for vanilla-js.
<br>
<a href="https://gitlab.com/aiocat/ccgo" target="_blank">CCGO:</a> Cross-platform compile tool for Go.
<br>
<a href="https://gitlab.com/aiocat/exfs" target="_blank">EXFS:</a> Bloat-free, FOOS regex file search tool.
<br>`)
}

function removeWarn(e) {
    e.target.remove()
}

document.getElementsByClassName("warn")[0].onclick = removeWarn